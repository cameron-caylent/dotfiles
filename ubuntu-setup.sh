#!/bin/bash
# setting up a new *buntu/debian machine
# should be idempotent so it can be run multiple times

echo "-----apt update-----"
sudo apt update

echo "-----apt upgrade-----"
sudo apt upgrade -y

echo "-----add repos-----"

echo "-----install stuff-----"
sudo apt update
sudo apt install -y vim htop curl git wget lm-sensors pv jq cmdtest python3 python3-pip npm ruby-full rbenv tmux gnupg rng-tools
# run lm-sensors with `sensors` or `sudo sensors` or `sudo watch -n 30 sensors`
# cmdtest == yarn
# gnupg == GPG
# to set up GPG
# https://help.ubuntu.com/community/GnuPrivacyGuardHowto
# to generate randomness 
# https://nsrc.org/workshops/2014/sanog23-security/raw-attachment/wiki/Agenda/2-1-1.pgp-lab.html
echo "-----generating randomness for PGP and starting the service-----"
sudo sed -i -e 's|#HRNGDEVICE=/dev/hwrng|HRNGDEVICE=/dev/urandom|' /etc/default/rng-tools
sudo service rng-tools start

# note that from Zach's script for osx, rectangle is an osx thing
# iterm2 is an osx thing
# bitwarden is a password manager, not sure if we're standardizing that but it is on Linux

###############
# Install Slack
###############
# https://slack.com/help/articles/212924728-Download-Slack-for-Linux--beta-

# for the record, I hate snap
sudo snap install slack --classic


####################
# Install virtualenv
####################
# https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/

echo "-----installing virtualenv-----"
python3 -m pip install --user virtualenv
  

#################
# Install AWS CLI
#################
# https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html

echo "-----Time to install AWS CLI!-----"
sudo apt update

echo "-----is curl installed?-----"
which curl
if [ $? -eq 1 ]
then
    sudo apt install curl -y
fi

echo "-----is aws cli installed?-----"
which aws
if [ $? -eq 1 ]
then
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
fi

aws --version


#################
# Install AWS CDK
#################
# https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html

echo "-----we're gonna see if npm is installed-----"
which npm
if [ $? -eq 1 ]
then
    echo "-----NPM was not installed. Installing...-----"
    sudo apt install npm -y
fi

echo "-----okay now using NPM to install the AWS CDK-----"
which cdk
if [ $? -eq 1 ]
then
    npm install aws-cdk
fi

cdk --version

################
# Install Docker
################
# https://docs.docker.com/engine/install/ubuntu/
# 

echo "-----Time to install Docker! Wooo!-----"
echo "-----We're going to prep everything now-----"
# make sure old versions are removed

which docker
if [ $? -eq 1 ]
then
    sudo apt-get remove docker docker-engine docker.io containerd runc
    # update & install stuff
    sudo apt update
    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg \
        lsb-release
    # add Docker's official GPG key
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    # set up stable repository
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    echo "-----Double checking the fingerprint now-----"
    sudo apt-key fingerprint 0EBFCD88
    # update & install
    echo "-----Update & Install Docker------"
    sudo apt update
    sudo apt install docker-ce docker-ce-cli containerd.io
    echo "-----Now to test our install-----"
    sudo docker run hello-world
    echo "-----Adding local user to docker group------"
    sudo usermod -aG docker $USER
    echo "-----You should probably reset now-----"
fi

########################
# Install Docker Compose
########################
# https://docs.docker.com/compose/install/

echo "-----Now to install docker-compose-----"
which docker-compose
if [ $? -eq 1 ]
then
    # Note, please double check the current version at the URL above before continuing
    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    echo "-----Double checking this works-----"
    echo "-----You should run `docker-compose --version` after you reboot to test, too-----"
    sudo docker-compose --version
fi

###################
# Install Terraform
###################
# https://learn.hashicorp.com/tutorials/terraform/install-cli

which terraform
if [ $? -eq 1 ]
then
    echo "-----Now for Terraform-----"
    sudo apt update
    sudo apt install -y gnupg software-properties-common
    curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
    sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
    sudo apt update
    sudo apt install terraform -y
    echo "-----did it work-----"
    terraform --help
    
    echo "~~~~~Note: To install Terraform Autocomplete:~~~~~"
    echo "~~~~~1. Make sure your ~/.**rc file exists for bash or zsh~~~~~"
    echo "~~~~~2. run `terraform -install-autocomplete`~~~~~"
    for I in 1 2 3 4 5
    do
        echo "$I / 5"
        sleep 1
    done
    echo "-----moving on...-----"
fi

##############
# Install Zoom
##############
# https://zoom.us/download?os=linux 

echo "-----we're gonna see if wget is installed-----"
which wget
if [ $? -eq 1 ]
then
    echo "-----wget was not installed. Installing-----"
    sudo apt install wget -y
fi

which zoom
if [ $? -eq 1 ]
then
    echo "-----downloading zoom-----"
    cd ~/Downloads
    # Ubuntu 64 bit 16.04+
    wget https://zoom.us/client/latest/zoom_amd64.deb
    # download public key
    wget https://zoom.us/linux/download/pubkey

    echo "-----installing zoom-----"
    sudo apt install ./zoom_amd64.deb
    sudo apt update 
fi

################
# Install Signal
################
# https://signal.org/en/download/#

# this will check if the signal repo has been added
# otherwise it'll make a mess
FILE=/etc/apt/sources.list.d/signal-xenial.list
if [ ! -f "$FILE" ]
then
    echo "-----installing Signal-----"
    wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
    cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
    sudo apt update
    sudo apt install signal-desktop
fi

################
# Install VSCode
################
# https://code.visualstudio.com/docs/setup/linux

echo "-----installing vscode-----"
# debian and ubuntu
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg

sudo apt install apt-transport-https
sudo apt update
sudo apt install code -y

################
# Install Pulumi
################
# https://www.pulumi.com/docs/get-started/install/

which pulumi
if [ $? -eq 1 ]
then
    echo "-----installing pulumi-----"
    curl -fsSL https://get.pulumi.com | sh
    echo "-----"
    echo "make sure you add ~/.pulumi/bin to your path!"
    echo "-----"    
    pulumi version
fi

#################
# Install Keybase
#################
# https://keybase.io/docs/the_app/install_linux

which keybase
if [ $? -eq 1 ]
then
    echo "-----installing keybase-----"
    curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb
    sudo apt install ./keybase_amd64.deb
    run_keybase
fi


#########
# cleanup
#########

echo "-----cleaning up-----"
sudo apt autoremove -y

